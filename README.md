<!--
layout: markdown_page
title: "[YOUR NAME]'s README"

--->

<!-- This template will help you build out your very own GitLab README, a great tool for transparently letting others know what it's like to work with you, and how you prefer to be communicated with. Each section is optional. You can remove those you aren't comfortable filling out, and add sections that are germane to you. --> 

## Justin's README

**Justin Mandell, UX Manager, Ops** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

**[GitLab Handle](https://gitlab.com/jmandell)**

## About me

* I grew up in the northwest suburbs of Chicago but decided to move out to southern California after my beard froze while waiting for the bus to get to work. And then a couple of years later I moved up to the San Bernardino mountains (5500 ft up) where it snows anyway!! Go Figure. Honestly, I had a hard time wth the lack of real seasons and if I ever get too cold I can just drive down to Redlands where it's nice and warm for a few hours :)
* I have a BS in Psychology and an MS in Human-computer Interaction
* I started out by studying music/composition with the lofty dream of becoming a movie score composer, but quickly realized a background in hard rock guitar wasn't the best foundation...
* I've always been interested in creating things to be used or consumed by others with a nice balance of form and function
* I've been designing in some form or another since 1999
* I love movies, video games and music

## My working style

* I love the discovery phase of a project. To me it's the most important part. If you can truly understand where your users are coming from, what their goals are... you can create something wonderful.
* I tend to be quiet, listening to what others have to say, only interjecting if I have something to add
* I'm not really a morning person but I start my day early anyway (dog wakes me up at 6:00am for a walk)
* Then my biggest problem in this regard is that I tend to stay up too late!!
* I don't have any difficulty getting into flow or staying on task and I love a good to-do list
* I love receiving feedback... anything that helps you grow is good to me!
* I believe team-work and collaboration is key

## My managing style

* All my life I've been a helper and/or a teacher. I enjoy it, a lot. I think that's a primary role of a manager
* I believe that my team was hired because they're good at what they do, so I should get out of their way and let them do it
* If I can help in anyway... awesome, tell me what you need!
* I believe that part of my job is to ensure they're able to do their job to the best of their ability, removing roadblocks and providing bridges
* I'm a strong beliver in team-work and collaboration
* I believe that everyone has something to contribute and that everyone's thoughts and opinions matter
* I think clear, honest, constructive feedback is essential to a design team. I do everything I can to make the team feel comfortable giving and receiving it
* What we do is not art, though there is self expression involved, our primary goal is to build something for someone else to use and to satisfy their needs so they can achieve their goals.

## What I assume about others

* I assume that eveyrone is here trying to essentially achieve the same goal: to build a product that our users want to use
* I assume that we're all learning and striving to do the best we can

## Communicating with me

* If you need to get ahold of me I'd start with Slack, then email
* My availability tends to fall between 8am and 5pm (PST), I will respond to emergencies, but I do believe in work/life balance... there will always be work that needs to get done.
* Being a designer, I'm a fairly visual person
* For the most part, I'm usually around as I don't really travel
* If you want me to review something in an Issue or MR please Ping me, otherwise I'll probably miss it
